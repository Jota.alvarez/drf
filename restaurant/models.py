from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

# Models of users
from django.db.models import Q, Sum


class Client(models.Model):
    class Meta:
        db_table = 'client'

    name = models.CharField(max_length=50, null=False)
    last_name = models.CharField(max_length=50, null=False)
    observations = models.CharField(max_length=255, blank=True, null=True)
    delete_client = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.name} {self.last_name}'


# Model waiter
class Waiter(models.Model):
    class Meta:
        db_table = 'waiter'

    name = models.CharField(max_length=50, null=False)
    last_name1 = models.CharField(max_length=50, null=False)
    last_name2 = models.CharField(max_length=50, null=False)
    delete_waiter = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.name} {self.last_name1}'


# model TypeProduct
class TypeProduct(models.Model):
    class Meta:
        db_table = 'type product'

    TYPE = (
        ('DRINK', 'Drink'),
        ('DISH', 'Dish'),
        ('DESERT', 'Desert'),
        ('ENTRY', 'Entry'),
    )
    type = models.CharField(max_length=6, choices=TYPE)
    description = models.CharField(max_length=255, null=False, blank=False)
    delete_type = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.type}'


# Products of menu
class Products(models.Model):
    class Meta:
        db_table = 'products'

    name = models.CharField(max_length=30, null=False, blank=False)
    description = models.CharField(max_length=255, null=False, blank=False)
    price = models.FloatField()
    stock = models.IntegerField()
    id_type_product = models.ForeignKey(TypeProduct, on_delete=models.CASCADE, null=False, blank=False)
    delete_products = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.name}'


# model of table
class Table(models.Model):
    class Meta:
        db_table = 'table'

    client_number = models.IntegerField()
    code = models.CharField(max_length=4, null=False)
    location = models.CharField(max_length=30, null=False)
    delete_table = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.code}'


# model invoice
class Invoice(models.Model):
    class Meta:
        db_table = 'invoice'

    date = models.DateTimeField(auto_now_add=True)
    id_client = models.ForeignKey(Client, on_delete=models.CASCADE, null=False)
    id_waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE, null=False)
    id_table = models.ForeignKey(Table, on_delete=models.CASCADE, null=False)
    delete_invoice = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.id_client}'


# model order
class Order(models.Model):
    class Meta:
        db_table = 'order'

    quantity = models.IntegerField()
    id_products = models.ForeignKey(Products, on_delete=models.CASCADE, null=False, blank=False)
    id_invoice = models.ForeignKey(Invoice, related_name='orders', on_delete=models.CASCADE)
    delete_order = models.BooleanField(default=False)

    def sub_price(self):
        price = self.id_products.price
        sub_total = self.quantity * price
        return sub_total

    def total_invoice(self):

        if self.id_invoice.id == self.id:
            filter_invoice = Order.objects.filter(Q(delete_order=False) & Q(id=self.id_invoice.id))\
                .aggregate(Sum('sub_total'))
            full_invoice = filter_invoice['sub_total__sum']

            return full_invoice

    def __str__(self):
        return f'{self.quantity}'
