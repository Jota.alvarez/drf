from django.contrib import admin

from restaurant.models import Client, Waiter, Table, Invoice, TypeProduct, Products, Order


# Register your models here.

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    search_fields = (
        'name',
    )


@admin.register(Waiter)
class WaiterAdmin(admin.ModelAdmin):
    search_fields = (
        'name',
    )


# Models products
@admin.register(TypeProduct)
class TypeProductAdmin(admin.ModelAdmin):
    search_fields = (
        'type',
    )


@admin.register(Products)
class ProductsAdmin(admin.ModelAdmin):
    search_fields = (
        'name',
    )


# Model of table
@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    search_fields = (
        'location',
    )


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


# Model of invoice 
@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    search_fields = (
        'id_client__name',
    )
