# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, TemplateView, DetailView
from rest_framework import viewsets, permissions

from restaurant.models import Client, Waiter, TypeProduct, Products, Table, Invoice, Order

# Home Page
from restaurant.serializers import ClientSerializer, WaiterSerializer, TypeProductSerializer, ProductsSerializer, \
    TableSerializer, InvoiceSerializer, OrderSerializer


# ------------------------- Home Page -------------------------------
class HomePageView(TemplateView):
    template_name = "home.html"


# ------------------------- Detail View -------------------------------
class DetailViewInvoice(DetailView):
    model = Invoice
    context_object_name = 'obj'
    template_name = 'invoice/invoice.html'


# ------------------------- Views Client -------------------------------
class CreateClient(CreateView):
    model = Client
    fields = ['name', 'last_name', 'observations']
    template_name = 'client/create_client.html'
    success_url = reverse_lazy('restaurant:list_client')


class ListClient(ListView):
    queryset = Client.objects.filter(delete_client=False)
    template_name = 'client/list_client.html'
    paginate_by = 10


class UpdateClient(UpdateView):
    model = Client
    fields = ['name', 'last_name', 'observations']
    template_name = 'client/update_client.html'
    success_url = reverse_lazy('restaurant:list_client')


class DeleteClient(UpdateView):
    model = Client
    fields = ['delete_client']
    template_name = 'client/delete_client.html'
    success_url = reverse_lazy('restaurant:list_client')


# ------------------------- Views Waiter -------------------------------
class CreateWaiter(CreateView):
    model = Waiter
    fields = ['name', 'last_name1', 'last_name2']
    template_name = 'waiter/create_waiter.html'
    success_url = reverse_lazy('restaurant:list_waiter')


class ListWaiter(ListView):
    queryset = Waiter.objects.filter(delete_waiter=False)
    template_name = 'waiter/list_waiter.html'
    paginate_by = 10


class UpdateWaiter(UpdateView):
    model = Waiter
    fields = ['name', 'last_name1', 'last_name2']
    template_name = 'waiter/update_waiter.html'
    success_url = reverse_lazy('restaurant:list_waiter')


class DeleteWaiter(UpdateWaiter):
    model = Waiter
    fields = ['delete_waiter']
    template_name = 'waiter/delete_waiter.html'
    success_url = reverse_lazy('restaurant:list_waiter')


# ------------------------- Views Type Products -------------------------------
class CreateTypeProduct(CreateView):
    model = TypeProduct
    fields = ['type', 'description']
    template_name = 'type/create_type.html'
    success_url = reverse_lazy('restaurant:list_type')


class ListTypeProduct(ListView):
    queryset = TypeProduct.objects.filter(delete_type=False)
    template_name = 'type/list_type.html'
    paginate_by = 10


class UpdateTypeProduct(UpdateView):
    model = TypeProduct
    fields = ['type', 'description']
    template_name = 'type/update_type.html'
    success_url = reverse_lazy('restaurant:list_type')


class DeleteTypeProduct(UpdateView):
    model = TypeProduct
    fields = ['delete_type']
    template_name = 'type/delete_type.html'
    success_url = reverse_lazy('restaurant:list_type')


# --------------------------- views Products --------------------------------
class CreateProducts(CreateView):
    model = Products
    fields = ['name', 'description', 'price', 'stock', 'id_type_product']
    template_name = 'product/create_product.html'
    success_url = reverse_lazy('restaurant:list_products')


class ListProducts(ListView):
    queryset = Products.objects.filter(delete_products=False)
    template_name = 'product/list_product.html'
    paginate_by = 10


class UpdateProducts(UpdateView):
    model = Products
    fields = ['name', 'description', 'price', 'stock', 'id_type_product']
    template_name = 'product/update_product.html'
    success_url = reverse_lazy('restaurant:list_products')


class DeleteProducts(UpdateView):
    model = Products
    fields = ['delete_products']
    template_name = 'product/delete_product.html'
    success_url = reverse_lazy('restaurant:list_products')


# --------------------------- views Table --------------------------------
class CreateTable(CreateView):
    model = Table
    fields = ['client_number', 'code', 'location']
    template_name = 'table/create_table.html'
    success_url = reverse_lazy('restaurant:list_table')


class ListTable(ListView):
    queryset = Table.objects.filter(delete_table=False)
    template_name = 'table/list_table.html'
    paginate_by = 10


class UpdateTable(UpdateView):
    model = Table
    fields = ['client_number', 'code', 'location']
    template_name = 'table/update_table.html'
    success_url = reverse_lazy('restaurant:list_table')


class DeleteTable(UpdateView):
    model = Table
    fields = ['delete_table']
    template_name = 'table/delete_table.html'
    success_url = reverse_lazy('restaurant:list_table')


# --------------------------- views Invoice --------------------------------
class CreateInvoice(CreateView):
    model = Invoice
    fields = ['id_client', 'id_waiter', 'id_table']
    template_name = 'invoice/create_invoice.html'
    success_url = reverse_lazy('restaurant:list_invoice')


class ListInvoice(ListView):
    queryset = Invoice.objects.filter(delete_invoice=False)
    template_name = 'invoice/list_invoice.html'
    paginate_by = 10


class UpdateInvoice(UpdateView):
    model = Invoice
    fields = ['id_client', 'id_waiter', 'id_table']
    template_name = 'invoice/update_invoice.html'
    success_url = reverse_lazy('restaurant:list_invoice')


class DeleteInvoice(UpdateView):
    model = Invoice
    fields = ['delete_invoice']
    template_name = 'invoice/delete_invoice.html'
    success_url = reverse_lazy('restaurant:list_invoice')


# --------------------------- views Order--------------------------------
class CreateOrder(CreateView):
    model = Order
    fields = ['quantity', 'id_products', 'id_invoice']
    template_name = 'order/create_order.html'
    success_url = reverse_lazy('restaurant:list_order')


class ListOrder(ListView):
    queryset = Order.objects.filter(delete_order=False)
    template_name = 'order/list_order.html'
    paginate_by = 10


class UpdateOrder(UpdateView):
    model = Order
    fields = ['quantity', 'id_products', 'id_invoice']
    template_name = 'order/update_order.html'
    success_url = reverse_lazy('restaurant:list_order')


class DeleteOrder(UpdateView):
    model = Order
    fields = ['delete_order']
    template_name = 'order/delete_order.html'
    success_url = reverse_lazy('restaurant:list_order')


# --------------------------- views Serializer --------------------------------
class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [permissions.IsAuthenticated]


class WaiterViewSet(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer
    permission_classes = [permissions.IsAuthenticated]


class TypeProductViewSet(viewsets.ModelViewSet):
    queryset = TypeProduct.objects.all()
    serializer_class = TypeProductSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProductsViewSet(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductsSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer
    permission_classes = [permissions.IsAuthenticated]


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    permission_classes = [permissions.IsAuthenticated]


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
