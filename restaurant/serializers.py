from rest_framework import serializers

from restaurant.models import Client, Waiter, TypeProduct, Products, Table, Invoice, Order


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['name', 'last_name', 'observations']
        #url es la url que se visualisa en la pantalla y muestra la info que se quiere serializarr
        #URL_FIELDS_NAME = ['new_url']
        #read_only_fields = [''fields]


class WaiterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waiter
        fields = ['name', 'last_name1', 'last_name2']


class TypeProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypeProduct
        fields = ['url', 'type', 'description']


class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ['name', 'description', 'price', 'stock', 'id_type_product']


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table
        field = ['client_number', 'code', 'location']


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = ['date', 'id_client', 'id_waiter', 'id_table']


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['quantity', 'id_products', 'id_invoice']



