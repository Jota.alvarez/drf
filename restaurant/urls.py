from django.urls import path, include
from rest_framework import routers

from restaurant.views import CreateClient, ListClient, UpdateClient, DeleteClient, HomePageView, CreateWaiter, \
    ListWaiter, DeleteWaiter, CreateTypeProduct, ListTypeProduct, UpdateTypeProduct, DeleteTypeProduct, CreateProducts, \
    ListProducts, DeleteProducts, UpdateProducts, CreateTable, ListTable, UpdateTable, DeleteTable, CreateInvoice, \
    ListInvoice, UpdateInvoice, DeleteInvoice, CreateOrder, ListOrder, UpdateOrder, DeleteOrder, UpdateWaiter, \
    DetailViewInvoice, ClientViewSet, WaiterViewSet, TypeProductViewSet, ProductsViewSet, TableViewSet, InvoiceViewSet, \
    OrderViewSet

app_name = 'restaurant'

router = routers.DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'waiter', WaiterViewSet)
router.register(r'type products', TypeProductViewSet)
router.register(r'products', ProductsViewSet)
router.register(r'table', TableViewSet)
router.register(r'invoice', InvoiceViewSet)
router.register(r'order', OrderViewSet)


urlpatterns = [
    path('', HomePageView.as_view(), name='home'),

    path('serializer/', include(router.urls), name='api'),

    # ---------------------------Urls Client--------------------------------
    path('create-client/', CreateClient.as_view(), name='create_client'),
    path('list-client/', ListClient.as_view(), name='list_client'),
    path('update-client/<int:pk>/', UpdateClient.as_view(), name='update_client'),
    path('delete-client/<int:pk>', DeleteClient.as_view(), name='delete_client'),

    # ---------------------------Urls Waiter--------------------------------
    path('create-waiter/', CreateWaiter.as_view(), name='create_waiter'),
    path('list-waiter/', ListWaiter.as_view(), name='list_waiter'),
    path('update-waiter/<int:pk>/', UpdateWaiter.as_view(), name='update_waiter'),
    path('delete-waiter/<int:pk>/', DeleteWaiter.as_view(), name='delete_waiter'),

    # ---------------------------Urls TypeProduct--------------------------------
    path('create-type/', CreateTypeProduct.as_view(), name='create_type'),
    path('list-type/', ListTypeProduct.as_view(), name='list_type'),
    path('update-type/<int:pk>/', UpdateTypeProduct.as_view(), name='update_type'),
    path('delete-type/<int:pk>/', DeleteTypeProduct.as_view(), name='delete_type'),

    # ---------------------------Urls Products--------------------------------
    path('create-products/', CreateProducts.as_view(), name='create_products'),
    path('list-products/', ListProducts.as_view(), name='list_products'),
    path('update-products/<int:pk>/', UpdateProducts.as_view(), name='update_products'),
    path('delete-products/<int:pk>/', DeleteProducts.as_view(), name='delete_products'),

    # ---------------------------Urls Table--------------------------------
    path('create-table/', CreateTable.as_view(), name='create_table'),
    path('list-table/', ListTable.as_view(), name='list_table'),
    path('update-table/<int:pk>/', UpdateTable.as_view(), name='update_table'),
    path('delete-table/<int:pk>/', DeleteTable.as_view(), name='delete_table'),

    # ---------------------------Urls Invoice--------------------------------
    path('create-invoice/', CreateInvoice.as_view(), name='create_invoice'),
    path('list-invoice/', ListInvoice.as_view(), name='list_invoice'),
    path('update-invoice/<int:pk>/', UpdateInvoice.as_view(), name='update_invoice'),
    path('delete-invoice/<int:pk>/', DeleteInvoice.as_view(), name='delete_invoice'),
    path('<int:pk>/', DetailViewInvoice.as_view(), name='detail_invoice'),

    # ---------------------------Urls Order--------------------------------
    path('create-order/', CreateOrder.as_view(), name='create_order'),
    path('list-order/', ListOrder.as_view(), name='list_order'),
    path('update-order/<int:pk>/', UpdateOrder.as_view(), name="update_order"),
    path('delete-order/<int:pk>/', DeleteOrder.as_view(), name="delete_order"),
]



